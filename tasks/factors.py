def factors(number):
    list = []
    for x in range(number):
        if x == 0:
            continue
        elif x == 1:
            continue
        else:
            if number % x == 0:
                    list.append(x)
    if len(list) == 0:
        return str(number) + " is a prime number"
    else:
        return list

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
