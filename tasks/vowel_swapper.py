def vowel_swapper(string):
    new_list = list(string)
    new_string_list = []
    a_count = 0
    e_count = 0
    i_count = 0
    o_count = 0
    u_count = 0
    for x in new_list:
        if x == 'a' or x == 'A':
            a_count += 1
            if a_count == 2:
                new_string_list.append('4')
            else:
                new_string_list.append(x)
        elif x == 'e' or x == 'E':
            e_count += 1
            if e_count == 2:
                new_string_list.append('3')
            else:
                new_string_list.append(x)
        elif x == 'i' or x == 'I':
            i_count += 1
            if i_count == 2:
                new_string_list.append('!')
            else:
                new_string_list.append(x)
        elif x == 'o':
            o_count += 1
            if o_count == 2:
                new_string_list.append('ooo')
            else:
                new_string_list.append(x)
        elif x == 'O':
            o_count += 1
            if o_count == 2:
                new_string_list.append('000')
            else:
                new_string_list.append(x)
        elif x == 'u' or x == 'U':
            u_count += 1
            if u_count == 2:
                new_string_list.append('|_|')
            else:
                new_string_list.append(x)
        else:
            new_string_list.append(x)
    new_string = ''.join(new_string_list)
    return new_string

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
