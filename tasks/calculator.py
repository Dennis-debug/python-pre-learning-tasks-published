import operator

#define operators you wanna use
allowed_operators={
	"+": operator.add,
	"-": operator.sub,
	"*": operator.mul,
	"/": operator.truediv}

def calculator(a, b, input):
	result = int(allowed_operators[input](a, b))
	return bin(result)[2:]

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should print 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
